package br.ucsal.bes20201.testequalidade.locadora;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;

public class ClienteBuilder {

	private static final String DEFAULT_CPF = "XXX-0000";
	private static final String DEFAULT_NOME = "Cliente";
	private static final String DEFAULT_TELEFONE = "3333-9999";

	private String cpf = DEFAULT_CPF;
	private String nome = DEFAULT_NOME;
	private String telefone = DEFAULT_TELEFONE;

	private ClienteBuilder() {

	}

	public static ClienteBuilder umCliente() {
		return new ClienteBuilder();
	}

	public ClienteBuilder comCPF(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public ClienteBuilder mas() {
		return ClienteBuilder.umCliente().comCPF(cpf).comNome(nome).comTelefone(telefone);

	}

	public Cliente build() {
		Cliente cliente = new Cliente(cpf, nome, telefone);
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		return cliente;
	}

}
