package br.ucsal.bes20201.testequalidade.locadora;

import java.time.LocalDateTime;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String DEFAULT_PLACA = "XXX-0000";

	private static final LocalDateTime DEFAULT_DATAHORA = LocalDateTime.now();

	// private static final Modelo DEFAULT_MODELO = Modelo;

	private static final SituacaoVeiculoEnum DEFAULT_SITUACAOVEICULO = SituacaoVeiculoEnum.DISPONIVEL;
	private static final Double DEFAULT_VALORDIARIA = 50d;

	private String placa = DEFAULT_PLACA;
	private LocalDateTime dataHora = DEFAULT_DATAHORA;
	// private Modelo modelo = DEFAULT_MODELO;
	private SituacaoVeiculoEnum situacaoVeiculoEnum = DEFAULT_SITUACAOVEICULO;
	private Double valorDiaria = DEFAULT_VALORDIARIA;

	private VeiculoBuilder() {

	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comDataHora(Integer ano, Integer mes, Integer dia, Integer hora, Integer minuto) {
		this.dataHora = LocalDateTime.of(ano, mes, dia, hora, minuto);
		return this;
	}

	private VeiculoBuilder comDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
		return this;
	}
//
//	public VeiculoBuilder comvalorDiariao(Double valorDiaria) {
//		this.valorDiaria = valorDiaria;
//		return this;
//	}

	public VeiculoBuilder mas() {
		return VeiculoBuilder.umVeiculo().comDataHora(dataHora).comPlaca(placa);
//		VeiculoBuilder novoVeiculoBuilder = umVeiculo();
//		novoVeiculoBuilder.ano = ano;
//		novoVeiculoBuilder.placa = placa;
//		novoVeiculoBuilder.situacaoVeiculoEnum = situacaoVeiculoEnum;
//		return novoVeiculoBuilder;
	}

	public Veiculo build() {

		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacaoVeiculoEnum);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;

	}
}
